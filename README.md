# Pandemics plugin

Atom plugin for using [Pandemics](https://pandemics.gitlab.io) directly in Atom.

## Installation

Since Micro$oft bought back Github, the community package registry is not working anymore. The package therefore needs to be installed through the command line tool. After installing `apm` (in Atom, `Atom > Install Shell Commands`), open a terminal and type:

```
apm install https://gitlab.com/pandemics/atom
```

## Requirements

You will need Latex installed on your computer  to publish to pdf (see Pandemics documentation).

## Usage

- Open your markdown document
- `ctrl+cmd+p` to publish with Pandemics
