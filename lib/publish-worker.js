const pandemics = require('pandemics');
const fs = require('fs');
const path = require('path');

let logs = '';

function local_logger (log_message) {
  logs += '\n' + log_message;
}

pandemics.publish({
  source: process.argv[2],
  logger: local_logger
})
.then((res) => {
  console.log('ok')
  process.exit(0);
})
.catch((err) => {
  console.log('error')
  local_logger(err.message)
  var logFile = path.join(path.dirname(process.argv[2]), 'pandemics',  path.basename(process.argv[2]) + '.log');
  process.send({logfile: logFile});
  fs.writeFileSync (logFile, logs);
  process.exit(1);
});
